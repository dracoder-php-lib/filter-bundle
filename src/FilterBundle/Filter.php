<?php


namespace Dracoder\FilterBundle;


use Doctrine\ORM\QueryBuilder;

/**
 * Class Filter
 *
 * @package App\Util
 */
class Filter
{
    /** @var int $operation */
    private $operation = Operation::EQUAL;
    /** @var array $parameters */
    private $parameters = [];
    /** @var bool $null */
    private $null = false;
    /** @var string $alias */
    private $alias = false;
    /** @var string $field */
    private $field;

    /**
     * @param string $field
     * @param int $operation
     * @param string|array $parameters
     */
    public function __construct(string $field, int $operation, $parameters = [])
    {
        $this->setField($field);
        if (is_array($parameters)) {
            $this->setParameters($parameters);
        } else {
            $this->setParameters([$parameters]);
        }
        $this->operation = $operation;
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     *
     * @return Filter
     */
    public function setOperation($operation): Filter
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     *
     * @return Filter
     */
    public function setParameters(array $parameters): Filter
    {
        $arrParameters = [];
        foreach ($parameters as $parameter) {
            if ($parameter !== 'null') {
                $arrParameters[] = $parameter;
            } else {
                $this->null = true;
            }
        }
        $this->parameters = $arrParameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     *
     * @return Filter
     */
    public function setAlias($alias): Filter
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     *
     * @return Filter
     */
    public function setField($field)
    {
        $arrField = explode('.', $field);
        if (sizeof($arrField) === 1) {
            $this->field = $field;
        } else {
            $this->alias = $arrField[0];
            $this->field = $arrField[1];
        }

        return $this;
    }

    /**
     * @param QueryBuilder $qb
     */
    public function apply(QueryBuilder &$qb)
    {
        $alias = ($this->alias) ? $this->alias : $qb->getRootAliases()[0];
        $parameters = $this->parameters;
        $parameterName = $this->field . $this->operation;

        switch ($this->operation) {
            case Operation::EQUAL:
                if ($this->null) {
                    $qb->andWhere($alias . '.' . $this->field . ' is null');
                } else {
                    $qb->andWhere($alias . '.' . $this->field . '=:' . $parameterName);
                }
                break;
            case Operation::NOT_EQUAL:
                if ($this->null) {
                    $qb->andWhere($alias . '.' . $this->field . ' is not null');
                } else {
                    $qb->andWhere($alias . '.' . $this->field . '<>:' . $parameterName);
                }
                break;
            case Operation::IN:
                if ($this->null) {
                    if ($parameters) {
                        $qb->andWhere($alias . '.' . $this->field . ' is null or ' . $alias . '.' . $this->field . ' in (:' . $parameterName . ')');
                    } else {
                        $qb->andWhere($alias . '.' . $this->field . ' is null ');
                    }
                } else {
                    $qb->andWhere($alias . '.' . $this->field . ' in (:' . $parameterName . ')');
                }
                break;
            case Operation::LIKE:
                $qb->andWhere($alias . '.' . $this->field . ' like :' . $parameterName);
                break;
            case Operation::CONTAINS:
                $qb->innerJoin($alias . '.' . $this->field, $this->field, 'with', $this->field . '.id in (:' . $parameterName . ')');
                break;
            case Operation::GREATER_OR_EQUALS_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' >= :' . $parameterName);
                break;
            case Operation::GREATER_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' > :' . $parameterName);
                break;
            case Operation::LESSER_OR_EQUALS_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' <= :' . $parameterName);
                break;
            case Operation::LESSER_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' < :' . $parameterName);
                break;
            case Operation::BETWEEN:
                $qb->andWhere($alias . '.' . $this->field . ' BETWEEN :' . $parameterName . 'start AND :' . $parameterName . 'end');
                $qb->setParameter($parameterName . 'start', $parameters[0]);
                $qb->setParameter($parameterName . 'end', $parameters[1]);
                $parameters = [];
                break;
            case Operation::INNER_JOIN:
                $qb->innerJoin($alias . '.' . $this->field, $this->field);
                break;
            case Operation::LEFT_JOIN:
                $qb->leftJoin($alias . '.' . $this->field, $this->field);
                break;
            case Operation:: OR:
                $where = [];
                /** @var Filter $parametro */
                foreach ($parameters as $parametro) {
                    $where[] = $parametro->obtenerWhere($qb);
                }
                $qb->andWhere(implode(' or ', $where));
                $parameters = false;
                break;
        }
        if ($parameters) {
            $qb->setParameter($parameterName, $parameters);
        }
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return string
     */
    private function obtenerWhere(QueryBuilder &$qb)
    {
        $where = '';
        $alias = ($this->alias) ? $this->alias : $qb->getRootAliases()[0];
        $parameters = $this->parameters;
        $parameterName = $this->field . $this->operation;

        switch ($this->operation) {
            case Operation::EQUAL:
                if ($this->null) {
                    $where = $alias . '.' . $this->field . ' is null';
                } else {
                    $where = $alias . '.' . $this->field . '=:' . $parameterName;
                }
                break;
            case Operation::IN:
                if ($this->null) {
                    $where = $alias . '.' . $this->field . ' is null or ' . $alias . '.' . $this->field . ' in (:' . $parameterName . ')';
                } else {
                    $where = $alias . '.' . $this->field . ' in (:' . $parameterName . ')';
                }
                break;
            case Operation::LIKE:
                $where = $alias . '.' . $this->field . ' like :' . $parameterName;
                break;
            case Operation::GREATER_OR_EQUALS_THAN:
                $where = $alias . '.' . $this->field . ' >= :' . $parameterName;
                break;
            case Operation::GREATER_THAN:
                $where = $alias . '.' . $this->field . ' > :' . $parameterName;
                break;
            case Operation::LESSER_OR_EQUALS_THAN:
                $where = $alias . '.' . $this->field . ' <= :' . $parameterName;
                break;
            case Operation::LESSER_THAN:
                $where = $alias . '.' . $this->field . ' < :' . $parameterName;
                break;
            case Operation::BETWEEN:
                $where = $alias . '.' . $this->field . ' BETWEEN :' . $parameterName . '-start AND :' . $parameterName . '-end';
                $qb->setParameter($parameterName . 'start', $parameters[0]);
                $qb->setParameter($parameterName . 'end', $parameters[1]);
                $parameters = [];
                break;
        }
        if ($parameters) {
            $qb->setParameter($parameterName, $parameters);
        }

        return $where;
    }
}