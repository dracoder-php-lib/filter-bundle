<?php


namespace Dracoder\FilterBundle;

/**
 * Class Operation
 *
 * @package App\Util\Repository
 */
class Operation
{
    public const EQUAL = 1;
    public const IN = 2;
    public const LIKE = 3;
    public const CONTAINS = 4;
    public const GREATER_THAN = 5;
    public const LESSER_THAN = 6;
    public const GREATER_OR_EQUALS_THAN = 7;
    public const LESSER_OR_EQUALS_THAN = 8;
    public const BETWEEN = 9;
    public const INNER_JOIN = 10;
    public const LEFT_JOIN = 11;
    public const OR = 12;
    public const NOT_EQUAL = 13;
}